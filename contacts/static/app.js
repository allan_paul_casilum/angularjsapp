//initialize the app
var mainApp = angular.module("mainApp", ['ngRoute']);

//list of contact data
var contactListData = [
	{
		id: 1,
		name: 'Artchelyn Casilum',
		number: '1234567890',
		address: 'address 1'
	},
	{
		id: 2,
		name: 'Allan Paul Casilum',
		number: '1122334455',
		address: 'address 2'
	},
	{
		id: 3,
		name: 'Elisabeth Casilum',
		number: '111222',
		address: 'address 3'
	},
	{
		id: 4,
		name: 'Nethanel Casilum',
		number: '12123344',
		address: 'address 4'
	}
];
//create a app value
mainApp.value('listData', contactListData);

//create the route
mainApp.config(['$routeProvider', function($routeProvider){
	$routeProvider.
	when('/show/:contactId',{
		templateUrl: 'view.php',
		controller: 'contactControllerShow'
	}).
	otherwise({
		redirectTo: '/'
	});
}]);

//create the factory
mainApp.factory('contactFactory', function(){
	var factoryContact = {};
	
	factoryContact.getListData = function(_listData){
		return _listData;
	};
	
	factoryContact.createData = function(_name, _number, _address, _list_data){
		var _id = (_list_data.length + 1)
		var add = {
			id: _id,
			name: _name,
			number: _number,
			address: _address
		}
		return _list_data.push(add);
	};
	
	factoryContact.remove = function(_id, _list_data){
		console.log(_id);
		console.log(_list_data);
		for (var i=0; i < _list_data.length; i++) {
			if (_list_data[i].id == _id) {
				_list_data.splice(i, 1);
			}
		}
	};
	
	return factoryContact;
});

//create the service
mainApp.service('contactService', function(contactFactory){
	var _factory = contactFactory;
	
	this.getList = function(_listData){
		return contactFactory.getListData(_listData);
	};
	
	this.create = function(_name, _number, _address, _list_data){
		return contactFactory.createData(_name, _number, _address, _list_data);
	};
	
	this.remove = function(_id, _list_data){
		return contactFactory.remove(_id, _list_data);
	};
});

//create the controller
mainApp.controller('contactController', function($scope, $routeParams, contactService, listData){
	$scope.name = '';
	$scope.number = 0;
	$scope.address = '';
	$scope.params = $routeParams;
	//get the list
	$scope.list = function(){
		return contactService.getList(listData);
	};
	//add
	$scope.add = function(){
		return contactService.create($scope.name, $scope.number, $scope.address, listData);
	};
	//delete
	$scope.remove  = function(_id){
		console.log(_id);
		return contactService.remove(_id, listData);
	};
});

mainApp.controller('contactControllerShow', function($scope, $routeParams, contactService, listData){
	$scope.params = $routeParams;
	var show = [];
	for (var i=0; i < listData.length; i++) {
        if (listData[i].id == $routeParams.contactId) {
            show = listData[i];
        }
    }
    $scope.contactData = show;
});
