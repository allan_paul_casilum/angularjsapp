<div class="panel panel-default">
  <div class="panel-heading">View Contact</div>
  <div class="panel-body">
	  <form name="update" method="post" novalidate>
		<p>Id : {{contactData.id}}</p>
		<div class="control-group" ng-class="{true: 'error'}[submitted && update.email.$invalid]">
			<div class="form-group">
				<label class="control-label" for="name">Name</label>
				<input type="text" name="update_name" ng-model="update_name" class="form-control" required />
				<span class="help-inline" ng-show="submitted && update.update_name.$error.required">Required</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="number">Number</label>
				<input type="text" name="update_number" ng-model="update_number" class="form-control" required />
				<span class="help-inline" ng-show="submitted && update.update_number.$error.required">Required</span>
				<span class="help-inline" ng-show="submitted && update.update_number.$error.number">Only Number</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="update_address">Address</label>
				<input type="text" name="update_address" ng-model="update_address" class="form-control" />
			</div>
		</div>
		<input type="hidden" name="update_id" ng-model="update_id">
		<button ng-click="updateContact()" class="btn btn-default">Update</button>
	</form>
  </div>
</div>
