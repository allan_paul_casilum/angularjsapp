<?php
define('SEED_DB', false);
// Or if you just download the medoo.php into directory, require it with the correct path.
require  '../../lib/medoo/medoo.php';
 
$database = new medoo([
	// required
	'database_type' => 'mysql',
	'database_name' => 'test_angulardb',
	'server' => 'localhost',
	'username' => 'root',
	'password' => 'potpot',
	'charset' => 'utf8',
 
	// [optional]
	'port' => 3306,
 
	// [optional] Table prefix
	'prefix' => '',
 
	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
	'option' => [
		PDO::ATTR_CASE => PDO::CASE_NATURAL
	]
]);

if( SEED_DB ){
	$seed = array(
		array(
			'name' => 'Artchelyn Casilum',
			'number' => '1234567890',
			'address' => 'address 1'
		),
		array(
			'name' => 'Allan Paul Casilum',
			'number' => '1122334455',
			'address' => 'address 2'
		),
		array(
			'name' => 'Elisabeth Casilum',
			'number' => '111222',
			'address' => 'address 3'
		),
		array(
			'name' => 'Nethanel Casilum',
			'number' => '111222333',
			'address' => 'address 4'
		)
	);
	$database->insert("contacts", $seed);
}


