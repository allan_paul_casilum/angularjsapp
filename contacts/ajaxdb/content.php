<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>AngularJS Phone Contact APP - Static</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right hidden">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Phone Contact AJAXDB - MySQL</h3>
      </div>

      <div class="jumbotron">
			<div ng-app="mainApp">
			  <div ng-controller="contactController as c_ctrl">
				<div>
					<form name="form" method="post" novalidate>
						<h2>Add New</h2>
						<div class="control-group">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="contactName" class="form-control" ng-model="name" required>
								<div ng-show="form.$submitted || form.contactName.$touched">
								  <div ng-show="form.contactName.$error.required">Tell us your name.</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="number">Number</label>
								<input type="text" name="number" ng-model="number" class="form-control" required="" />
								<span class="help-inline" ng-show="form.$submitted && form.number.$error.required">Required</span>
								<span class="help-inline" ng-show="form.$submitted && form.number.$error.number">Only Number</span>
							</div>
							<div class="form-group">
								<label class="control-label" for="address">Address</label>
								<input type="text" name="address" ng-model="address" class="form-control" required="" />
								<span class="help-inline" ng-show="form.$submitted && form.address.$error.required">Required</span>
							</div>
						</div>
					   
						<button type="submit" ng-click="add()" class="btn btn-default">Create</button>
						<button type="button" ng-click="reset(form)" class="btn btn-default">Reset</button>
					</form>
				</div>
				<hr/>
				<div class="search-container">
					<form name="form" novalidate>
						<h2>Search name or number</h2>
						<div class="form-group">
						<input class="form-control" type="text" ng-model="search" placeholder="Search Name or Number">
						</div>
					</form>
				</div>
				<hr>
				<div ng-view></div>
				<h2>List of Contacts</h2>
				<ul class="list-group">
					<li class="list-group-item" ng-repeat="contactItem in items | orderBy:-id | filter: search">
						<p>Id : {{contactItem.id}}</p>
						<p>Name : {{contactItem.name}}</p>
						<p>Number : {{contactItem.number}}</p>
						<p>
							<a href="#show/{{contactItem.id}}">Show</a> -
							<a href="#" ng-click="remove(contactItem.id);">Delete</a>
						</p>
					</li>
				</ul>
			  </div>
		  </div>
      </div>

      <footer class="footer">
        <p>&copy; 2016 Company, Inc.</p>
      </footer>

    </div> <!-- /container -->

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-route.js"></script>
	<script src="app.js"></script>
  </body>
</html>

