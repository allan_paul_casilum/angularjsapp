var express = require('express');
var router = express.Router();

//instantiate model Person
var Person = require('../models/Person.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var persons_data = [];
	Person.find(function(err, personObj){
		if(err) return next(err);
		res.render('persons', {
		title: 'List of Persons Contact',
			persons:personObj
		});
	});
});

//Add Person
router.get('/addperson', function(req, res, next){
	res.render('addperson', { 
		title: 'Add Person' 
	});
});

//create the person
router.post('/create',function(req, res, next){
	Person.create(req.body, function(err, post){
		if(err)return next(err);
		res.redirect('/persons');
	});
});

//show
router.get('/show/:id',function(req, res, next){
	//res.json(req.params.id);
	Person.findById(req.params.id, function(err, showPerson){
		if(err)return next(err);
		res.render('showperson',{
			title:'Details',
			person:showPerson
		});
		//res.json(showPerson.name);
	});
});

//update
router.post('/update', function(req, res, next){
	//res.json(req.body);
	var _id = req.body.id;
	var _post_body = req.body;
	Person.findByIdAndUpdate(_id, _post_body, function(err, post){
		if(err)return next(err);
		res.redirect('/persons');
	});
});

//delete
router.get('/delete/:id', function(req, res, next){
	Person.findByIdAndRemove(req.params.id, function(err, remPerson){
		if(err)return next(err);
		res.redirect('/persons');
	});
});

module.exports = router;
