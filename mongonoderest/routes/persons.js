var express = require('express');
var router = express.Router();

//instantiate model Person
var Person = require('../models/persons.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var persons_data = [];
	Person.find(function(err, personObj){
		if(err) return next(err);
		res.json(personObj);
	});
});

/*CREATE*/
router.post('/create', function(req, res, next){
	var _res = {
		msg:'Not Created',
		data:null
	};
	Person.create(req.body, function(err, post){
		if(err){
			_res.msg = 'Error';
			_res.data = err;
		}else{
			_res.msg = 'Added Successfully';
			_res.data = post;
		}
		res.json(_res);
	});
});

/*UPDATE*/
router.put('/update/:id', function(req, res, next){
	var _res = {
		msg:'Not Updated',
		data:null
	};
	var _id = req.params.id;
	var _post_body = req.body;
	Person.findByIdAndUpdate(_id, _post_body, function(err, putUpdate){
		if(err){
			_res.msg = 'Error Update';
			_res.data = err;
		}else{
			_res.msg = 'Updated Successfully';
			_res.data = putUpdate;
		}
		res.json(_res);
	});
});

/*READ*/
router.get('/show/:id', function(req, res, next){
	var _res = {
		msg:'No Data',
		data:null
	};
	Person.findById(req.params.id, function(err, showPerson){
		if(err){
			_res.msg = 'Error, Not Found';
			_res.data = err;
		}else{
			_res.msg = 'Found Successfully';
			_res.data = showPerson;
		}
		res.json(_res);
	});
});

/*DELETE*/
router.delete('/delete/:id', function(req, res, next){
	var _res = {
		msg:'Not Deleted',
		data:null
	};
	Person.findByIdAndRemove(req.params.id, function(err, remPerson){
		if(err){
			_res.msg = 'Error Delete';
			_res.data = err;
		}else{
			_res.msg = 'Deleted Successfully';
			_res.data = remPerson;
		}
		res.json(_res);
	});
});

module.exports = router;
