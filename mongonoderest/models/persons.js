var mongoose = require('mongoose');

var PersonSchema = new mongoose.Schema({
	name:String,
	phone:String,
	address:String
},{collection:'person'});

module.exports = mongoose.model('Person', PersonSchema);
