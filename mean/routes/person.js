var express = require('express');
var router = express.Router();

//instantiate model Person
var Person = require('../models/Person.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var persons_data = [];
	Person.find(function(err, personObj){
		if(err) return next(err);
		res.json(personObj);
	});
});

//Add Person
router.get('/addperson', function(req, res, next){
	res.render('addperson', { 
		title: 'Add Person' 
	});
});

//create the person
router.post('/create',function(req, res, next){
	var _res = {
		msg:'Not Created',
		data:null,
		success:false
	};
	Person.create(req.body, function(err, post){
		if(err){
			_res.msg = 'Error';
			_res.data = err;
		}else{
			_res.msg = 'Added Successfully';
			_res.data = post;
			_res.success = true;
		}
		res.json(_res);
	});
});

//show
router.get('/show/:id',function(req, res, next){
	var _res = {
		msg:'No Data',
		data:null
	};
	Person.findById(req.params.id, function(err, showPerson){
		if(err){
			_res.msg = 'Error, Not Found';
			_res.data = err;
		}else{
			_res.msg = 'Found Successfully';
			_res.data = showPerson;
		}
		res.json(_res);
	});

});

//update
router.put('/update', function(req, res, next){
	var _res = {
		msg:'Not Updated',
		data:null,
		success:false
	};

	Person.findByIdAndUpdate(req.body.id, req.body, function(err, putUpdate){
		if(err){
			_res.msg = 'Error Update';
			_res.data = err;
		}else{
			_res.msg = 'Updated Successfully';
			_res.data = putUpdate;
			_res.success = true;
		}
		res.json(_res);
	});
});

//delete
router.delete('/delete/:id', function(req, res, next){
	var _res = {
		msg:'Not Deleted',
		data:null,
		success:false
	};
	Person.findByIdAndRemove(req.params.id, function(err, remPerson){
		if(err){
			_res.msg = 'Error Delete';
			_res.data = err;
		}else{
			_res.msg = 'Deleted Successfully';
			_res.data = remPerson;
			_res.success = true;
		}
		res.json(_res);
	});
});

module.exports = router;
