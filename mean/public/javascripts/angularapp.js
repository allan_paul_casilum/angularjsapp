//initialize the app
var mainApp = angular.module("mainApp", ['ngRoute']);

//list of contact data
var contactListData = [];
//create a app value
mainApp.value('listData', contactListData);

//create the route
mainApp.config(['$routeProvider', function($routeProvider){
	$routeProvider
	.when('/show/:id',{
		templateUrl: 'show.html',
		controller: 'contactControllerShow'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);

//create the factory
mainApp.factory('contactFactory', function($http, $q){
	var factoryContact = {};
	
	var _obj_toparams = function(_object){
		var str = "";
		for (var key in _object) {
			if (str != "") {
				str += "&";
			}
			str += key + "=" + encodeURIComponent(_object[key]);
		}
		return str;
	}
	
	factoryContact.getListData = function(){
		var deferred = $q.defer();
		$http({
			url:'/persons',
			method:'GET',
			params:{}
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	factoryContact.createData = function(_name, _number, _address){
		var deferred = $q.defer();
		
		var add = {
			name: _name,
			phone: _number,
			address: _address
		};
		$http({
			url:'/persons/create',
			method:'POST',
			data: _obj_toparams(add),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	//show by id
	factoryContact.show = function(_id){
		var deferred = $q.defer();
		var show_url = '/persons/show/' + _id;
		$http({
			url:show_url,
			method:'GET',
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	//update data
	factoryContact.updateData = function(_id, _name, _number, _address){
		var deferred = $q.defer();
		
		var update = {
			id: _id,
			name: _name,
			phone: _number,
			address: _address
		};
		console.log(update);
		$http({
			url:'/persons/update',
			method:'PUT',
			data: _obj_toparams(update),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	factoryContact.remove = function(_id){
		var deferred = $q.defer();
		
		var remove = {
			id: _id
		};
		var removeUrl = '/persons/delete/' + _id;
		/*$http.delete(removeUrl)
		.success(function(res){
			deferred.resolve(res);
		});*/
		$http({
			url:removeUrl,
			method:'DELETE'
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	return factoryContact;
});

//create the service
mainApp.service('contactService', function(contactFactory, $rootScope){
	var _factory = contactFactory;
	
	this.getList = function(){
		return _factory.getListData();
	};
	
	this.create = function(_name, _number, _address){
		return _factory.createData(_name, _number, _address);
	};

	this.update = function(_id, _name, _number, _address){
		return _factory.updateData(_id, _name, _number, _address);
	};
	
	this.remove = function(_id){
		return _factory.remove(_id);
	};
	
	this.show = function(_id){
		return _factory.show(_id);
	};
});

//create the controller
mainApp.controller('contactController', function($scope, $routeParams, contactService){
	$scope.name = '';
	$scope.number = 0;
	$scope.address = '';
	$scope.params = $routeParams;
	//$scope.update_name;
	
	//get the list
	$scope.items = [];
	var loadList = function(){
		contactService.getList().then(function(data){
			$scope.items = data;
		});
	};
	
	$scope.ngloadList = function(){
		loadList();
	};
	
	loadList();
		
	//add
	$scope.add = function(){
		var _ret = contactService.create($scope.name, $scope.number, $scope.address);
		$scope.reset();
		_ret.then(function(data){
			loadList();
		});
		if( _ret.success ){
			console.log('success');
		}
	};
	
	//delete
	$scope.remove  = function(_id){
		var _ret = contactService.remove(_id);
		_ret.then(function(data){
			loadList();
		});
	};
	
	$scope.reset = function(form){
	  if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
      $scope.name = '';
	  $scope.number = '';
	  $scope.address = '';
	};
	
	$scope.reset();
});

mainApp.controller('contactControllerShow', function($scope, $routeParams, contactService){
	$scope.contactData = [];
	$scope.params = $routeParams;
	
	var _current_id = $routeParams.id;
	
	contactService.show(_current_id).then(function(data){
		var _data = data.data;
		$scope.contactData = _data;
		$scope.update_id = _data._id;
		$scope.update_name = _data.name;
		$scope.update_number = _data.phone;
		$scope.update_address = _data.address;
	});
	
	//update
	$scope.updateContact = function(){
		var _ret = contactService.update($scope.update_id, 
		$scope.update_name, 
		$scope.update_number, 
		$scope.update_address);
		_ret.then(function(data){
			$scope.ngloadList();
		});
	}
});
